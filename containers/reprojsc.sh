#! /bin/bash
#
# Required Options:
# -a : arch (mips or arm)
# -b : build webkit
# -t : test webkit (assumes --build)
# -f  <regexp>: filter to be passed to the run-javascript-tests script
# Optional Options:
# -i : enters a terminal 
# Arguments:
# <path>: path to a local webkit checkout to use in building and testing
set -Eeuo pipefail

INTERACTIVE=0
BUILD=0
TEST=0
FILTER=""
ARCH=
while getopts ":a:ibtf:" opt; do
    case ${opt} in
	a)
	    ARCH="$OPTARG"
	    ;;
	i)
	    INTERACTIVE=1
	    ;;
	b)
	    BUILD=1
	    ;;
	t)
	    TEST=1
	    BUILD=1
	    ;;
	f)
	    TEST=1
	    BUILD=1
	    FILTER="$OPTARG"
	    ;;
	\?)
	    echo "Invalid option: $OPTARG" 1>&2
	    exit
	    ;;
	:)
	    echo "Invalid option: $OPTARG requires an argument" 1>&2
	    exit
	    ;;
    esac
done
shift $((OPTIND - 1))

if [[ $ARCH == '' ]]; then
    echo "Please specify -a <arch>"
    exit
fi

if [[ $# != 1 ]]; then
    echo "Not enough arguments"
    exit
fi
WEBKIT=$1

if [ ! -d $WEBKIT ]; then
    echo "Directory $WEBKIT does not exist"
    exit
fi

echo "Using WebKit path $WEBKIT"
echo "Plan:"
if [[ $BUILD == 1 ]]; then
    echo " * Build;"
fi
if [[ $TEST == 1 ]]; then
    echo " * Test;"
    if [[ $FILTER != "" ]]; then
	echo "   Using filter: \"$FILTER\""
    fi
fi
if [[ $INTERACTIVE == 1 ]]; then
    echo " * Going into interactive mode;"
fi

read -n1 -r -p "Press any key to continue or Ctrl-C to exit..." keyign

did=$(docker run --rm -di -v $WEBKIT:/WebKit pmatos/jsc32-base:$ARCH)

UNAME=
if [[ "$ARCH" == "arm" ]]; then
    UNAME="armv7l"
elif [[ "$ARCH" == "mips" ]]; then
    UNAME="mips"
fi
    

if [[ "$(docker exec $did uname -m)" != "$UNAME" ]]; then
    echo "Something is wrong - incorrect container architecture"
    docker stop $did
    exit
fi

[[ $BUILD == 1 ]] && docker exec $did /bin/bash -c 'cd /WebKit && Tools/Scripts/build-jsc --release --jsc-only'
[[ $TEST == 1 ]] && docker exec $did /bin/bash -c 'cd /WebKit && Tools/Scripts/run-javascriptcore-tests --no-build --no-fail-fast --json-output=jsc_results.json --release --memory-limited --no-testmasm --no-testair --no-testb3 --no-testdfg --no-testapi --jsc-only'
[[ $INTERACTIVE == 1 ]] && docker exec -ti $did /bin/bash
docker stop $did
